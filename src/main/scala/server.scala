package com.wiliot.training2

import java.sql.DriverManager
import java.sql.Connection
import com.wiliot.training2.postData
import com.wiliot.training2.Event
import com.wiliot.training2.MyActor
import akka.actor.typed.{ActorRef, Behavior}
import akka.actor.ActorSystem
import akka.actor.typed.scaladsl.Behaviors
import akka.actor.Props

import akka.http.scaladsl.Http
import akka.Done
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.model.StatusCodes

import scala.concurrent.Await
// for JSON serialization/deserialization following dependency is required:
// "com.typesafe.akka" %% "akka-http-spray-json" % "10.1.7"
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import spray.json.DefaultJsonProtocol._
import akka.actor.Props
import scala.io.StdIn

import scala.concurrent.Future

import akka.actor.{Actor, ActorLogging}




object Server {
  // needed to run the route
  implicit val system = ActorSystem( "SprayExample")
  // needed for the future map/flatmap in the end and future in fetchItem and saveOrder
  implicit val executionContext = system.dispatcher

  val myActor = system.actorOf(Props[MyActor], "name")
  val updateActor = system.actorOf(Props[MyActor], "update")

  // domain model

  // formats for unmarshalling and marshalling
  implicit val eventFormat = jsonFormat8(Event)

  def main(args: Array[String]): Unit = {
    // connect to the database named "mysql" on the localhost
    val driver = "com.mysql.jdbc.Driver"
    val url = "jdbc:mysql://10.10.10.240:3306/asset_tracking_training"
    val username = "root"
    val password = "MatiCaspi123!@"

    // there's probably a better way to do this
    var connection:Connection = null

    try {
      // make the connection
      Class.forName(driver)
      connection = DriverManager.getConnection(url, username, password)
      val statement = connection.createStatement()
    } catch {
      case e => e.printStackTrace
    }
    val route: Route =
      concat(
        get {
          pathPrefix("events" / LongNumber) { id =>
            complete("comp")
//            val resultSet = statement.executeQuery("SELECT id, name FROM asset")
//            // there might be no item for a given id
//            val maybeItem: Future[Option[Item]] = fetchItem(id)
//
//            onSuccess(maybeItem) {
//              case Some(item) => complete(item)
//              case None       => complete(StatusCodes.NotFound)
//            }
          }
        },
        post {
          path("event") {
            entity(as[Event]) { event =>

//              val saved: Future[Done] = saveDB(connection, event)
              myActor ! postData(connection, event)
              if (event.eventType == "ACTV")
              println("Done")
              complete(StatusCodes.OK)
            }
          }
        }
      )

    val bindingFuture = Http().newServerAt("localhost", 8080).bind(route)
    println(s"Server online at http://localhost:8080/\nPress RETURN to stop...")
    StdIn.readLine() // let it run until user presses return
    bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ => {
        system.terminate()
      }) // and shutdown when done
  }
}




object TestExample {

    def main(args: Array[String]) {
      // connect to the database named "mysql" on the localhost
      val driver = "com.mysql.jdbc.Driver"
      val url = "jdbc:mysql://10.10.10.240:3306/asset_tracking_training"
      val username = "root"
      val password = "MatiCaspi123!@"

      // there's probably a better way to do this
      var connection:Connection = null

      try {
        // make the connection
        Class.forName(driver)
        connection = DriverManager.getConnection(url, username, password)

        // create the statement, and run the select query
        val statement = connection.createStatement()
        val sql_ =
          s"""
             |select poi_id from event where event_timestamp=5
             |""".stripMargin
        val resultSet = statement.executeQuery(sql_)
        resultSet.next()
        val poi_id = resultSet.getString("poi_id")
        println(poi_id==null)
        println(poi_id)

      } catch {
        case e => e.printStackTrace
      }
      connection.close()
    }
}
