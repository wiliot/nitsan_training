package com.wiliot.training2

import akka.Done
import akka.actor.TypedActor.dispatcher
import akka.actor.{Actor, ActorLogging, ActorRef, Props}
import com.wiliot.training2.Event
import com.wiliot.training2.Server.{system, updateActor}

import java.sql.Connection
import scala.concurrent.Future

case class postData(connection: Connection, event: Event)
case class updateData(connection: Connection, sql:String )


class MyActor extends Actor with ActorLogging{
  def saveEvent(sender: ActorRef, data: postData): Future[Done] = {
    val conn = data.connection
    val event = data.event
    val statement = conn.createStatement()

    var poi_id: String = "NULL"

    // get asset data
    val sql_asset =
      s"""
         |select * from asset where tag_id='${event.tagId}'
         |""".stripMargin
    val resultSet = statement.executeQuery(sql_asset)
    var asset:Asset = null
    if ( resultSet.next() ) { // need to address the case where no asset is found
      val id = "'" + resultSet.getString("id") + "'"
//      match {
//        case `String => "'" + _ + "'"
//        case _ => "NULL"
//      }
      val name = "'" + resultSet.getString("name") + "'"
      val asset_type_id = "'" + resultSet.getString("asset_type_id") + "'"
//      val poi_id = "'" + resultSet.getString("poi_id") + "'"
      val project_id = "'" + resultSet.getString("project_id") + "'"
      asset = Asset(id = id, name, asset_type_id, project_id)
      if ( resultSet.next() ) println("Error: multiple assets for a tag id")
    } else {
      asset = Asset("NULL", "NULL", "NULL", "NULL")
    }

    //get poi
    val sql_poi =
      s"""
         |select poi_id from poi_association where association_type='gateway' and association_value='${event.gatewayId}'
         |""".stripMargin

    val resultSet_poi = statement.executeQuery(sql_poi)
    if ( resultSet_poi.next() ) {
      poi_id = "'" + resultSet_poi.getString("poi_id") + "'"
    }

    event.eventType match {
      case "ACTV" => {
        val new_status = event.value match {
          case 0 => "not active"
          case _ =>  "active"
        }
        println(new_status + asset.id)
        val sql_update =
          s"""
             |UPDATE asset
             |SET status = '${new_status}'
             |WHERE id=${asset.id};
             |""".stripMargin

        updateActor ! updateData(conn, sql_update)
      }
      case "LOCH" => {
        val sql_update =
          s"""
             |UPDATE asset
             |SET poi_id = $poi_id
             |WHERE id=${asset.id};
             |""".stripMargin
        updateActor ! updateData(conn, sql_update)
      }
      case _ => 0
    }
    val sql =
      s"""
         |insert into event
         |values ('${event.eventType}', ${event.eventTimestamp},'2021-07-14', ${asset.id}, ${asset.assetTypeId}, '${event.tagId}', $poi_id, '${event.gatewayId}', '${event.ownerId}', ${asset.projectId}, ${event.lat}, ${event.lng}, ${event.value})
         |""".stripMargin
    println(sql)
    val resultSet2 = statement.executeUpdate(sql)
    if (resultSet2==1) println("inserted successfuly") else println(resultSet2)
    Future{ Done }
  }
  def updateAsset(sender: ActorRef, data: updateData) = {
    println("sql_update")
    val conn = data.connection
    val statement = conn.createStatement()
    val resultSet2 = statement.executeUpdate(data.sql)
    if (resultSet2==1) println("updated successfuly") else println(resultSet2)
  }
  override def receive = {
    case req: postData => saveEvent(sender, req)
    case req: updateData => updateAsset(sender, req)
    case _ => println("??")
  }
}

