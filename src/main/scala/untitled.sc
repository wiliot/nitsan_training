import java.sql.{Connection, DriverManager}

// connect to the database named "mysql" on the localhost
val driver = "com.mysql.jdbc.Driver"
val url = "jdbc:mysql://10.10.10.240:3306/asset_tracking_training"
val username = "root"
val password = "MatiCaspi123!@"

// there's probably a better way to do this
var connection:Connection = null

try {
  // make the connection
  Class.forName(driver)
  connection = DriverManager.getConnection(url, username, password)

  // create the statement, and run the select query
  val statement = connection.createStatement()
  val sql_ =
    s"""
       |select * from asset where tag_id='blyott-8-02202'
       |""".stripMargin
  val resultSet = statement.executeQuery(sql_)
  println(resultSet)
} catch {
  case e => e.printStackTrace
}
connection.close()
