package com.wiliot.training2

//import SprayJsonExample.Event

import java.sql.Connection


final case class Event(eventTimestamp: Long,
                      eventType: String,
                      ownerId: String,
                      tagId: String,
                      gatewayId: String,
                      value: Double,
                      lng: Double,
                      lat: Double
                      )

final case class Asset(id: String,
                       name: String,
                       assetTypeId: String,
                       projectId: String
  //                       tagId: String,
//                       poiId: String,
//                       value: Double,
//                       lng: Double,
//                       lat: Double
                      )